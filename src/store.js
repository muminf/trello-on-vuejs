import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex);

export const store = new Vuex.Store({
     state: { 
      data: {
        stages: ['on-hold', 'in-progress', 'needs-review', 'approved'], 
        blocks: JSON.parse(localStorage.getItem('blocks')),
      }
    },
    getters: {},
    mutations: {
      deleteBlock: (state, payload) => {
        var id = state.data.blocks.findIndex(function(item, i){
          return item.id === Number(payload)
        })
        Vue.delete(state.data.blocks, id)
        localStorage.setItem('blocks', JSON.stringify(state.data.blocks))
      },
      addBlock: (state, payload) => {
        if(state.data.blocks === null){
            state.data.blocks = []
        }
        state.data.blocks.push(payload)
        localStorage.setItem('blocks', JSON.stringify(state.data.blocks))
      }
    },
    actions: {
    },
  })