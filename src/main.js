// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import {store} from './store.js'
import App from './App'
import vueKanban from 'vue-kanban'
import sass from 'sass-loader'


Vue.use(Vuex)
Vue.use(vueKanban)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  components: { App },
  template: '<App/>',
  /*beforeCreate: function () {
    //localStorage.setItem('blocks', JSON.stringify(this.$store.state.data.list));
    if(localStorage.getItem('blocks') === null){
      localStorage.setItem('blocks', JSON.stringify(this.$store.state.data.list));
    }
  },*/
})
